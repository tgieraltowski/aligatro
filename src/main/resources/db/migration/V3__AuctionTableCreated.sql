create table auction (
    id serial primary key,
    description text,
    auction_category varchar(30),
    picture bytea,
    minimum_bid double precision,
    buy_now_price double precision,
    promoted boolean,
    city varchar(40),
    start_date date,
    end_date date,
    views bigint
    );