create table users(
	username varchar(60) not null primary key,
	password varchar(60) not null,
	enabled boolean not null
);

create table authorities (
	username varchar(60) not null,
	authority varchar(60) not null,
	users_username varchar(60) not null,
	constraint fk_authorities_users foreign key(username) references users(username)
);
create unique index ix_auth_username on authorities (username,authority);