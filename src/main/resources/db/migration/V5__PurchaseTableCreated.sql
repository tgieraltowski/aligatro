CREATE TABLE purchase(
    id serial primary key,
    auction_id int references auction(id),
    users_username varchar(60) references users(username),
    amount double precision,
    purchase_date date
);

ALTER TABLE auction
ADD auction_status varchar(60);