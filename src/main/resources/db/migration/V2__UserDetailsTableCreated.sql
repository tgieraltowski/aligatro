create table user_details(
                      id serial primary key,
                      email varchar(60),
                      province varchar(40),
                      city varchar(40),
                      street varchar(40),
                      house_number int,
                      apartment_number int,
                      post_code varchar(10),
                      account_creation_date date,
                      account_status varchar(40),
                      account_type varchar(40),
                      users_username varchar(60),
                      avatar bytea
);