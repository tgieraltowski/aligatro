CREATE TABLE bidding(
    id serial primary key,
    current_bid double precision,
    users_username varchar(60) references users(username),
    auction_id int references auction(id)
)