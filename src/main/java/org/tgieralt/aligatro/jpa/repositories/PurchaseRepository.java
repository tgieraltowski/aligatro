package org.tgieralt.aligatro.jpa.repositories;

import org.springframework.data.repository.CrudRepository;
import org.tgieralt.aligatro.jpa.models.Purchase;

import java.util.List;

public interface PurchaseRepository extends CrudRepository<Purchase, Integer> {
    List<Purchase> findAllByUserUsername(String username);
}
