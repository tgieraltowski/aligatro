package org.tgieralt.aligatro.jpa.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.tgieralt.aligatro.jpa.models.UserDetails;

public interface UserDetailsRepository extends CrudRepository<UserDetails, Long> {

    @Query(value = "SELECT * FROM user_details WHERE users_username LIKE ?", nativeQuery = true)
    UserDetails findUsersDetailsByUsername(String username);
}
