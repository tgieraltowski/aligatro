package org.tgieralt.aligatro.jpa.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.tgieralt.aligatro.jpa.models.Auction;

import java.util.List;
import java.util.Optional;

public interface AuctionRepository extends CrudRepository<Auction, Integer> {
    List<Auction> findAllByUserUsername(String username);

    @Query(value = "SELECT * FROM auction WHERE upper(title) LIKE upper(concat('%',?,'%'))", nativeQuery = true)
    Optional<Auction> searchPhraseInTitle(String phrase);

    @Query(value = "SELECT * FROM auction WHERE upper(description) LIKE upper(concat('%',?,'%'))", nativeQuery = true)
    Optional<Auction> searchPhraseInDescription(String phrase);

    @Query(value = "SELECT * FROM auction WHERE auction_status = 'ACTIVE' AND end_date < now()", nativeQuery = true)
    List<Auction> findAllMaturedAuctions();
}
