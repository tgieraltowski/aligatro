package org.tgieralt.aligatro.jpa.repositories;

import org.springframework.data.repository.CrudRepository;
import org.tgieralt.aligatro.jpa.models.Authority;

public interface AuthorityRepository extends CrudRepository<Authority, String> {

}
