package org.tgieralt.aligatro.jpa.repositories;

import org.springframework.data.repository.CrudRepository;
import org.tgieralt.aligatro.jpa.models.User;

public interface UserRepository extends CrudRepository<User, String> {

}
