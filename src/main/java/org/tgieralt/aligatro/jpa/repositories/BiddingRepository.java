package org.tgieralt.aligatro.jpa.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.tgieralt.aligatro.jpa.models.Bidding;

public interface BiddingRepository extends CrudRepository<Bidding, Integer> {
    @Query(value = "SELECT * FROM bidding WHERE auction_id = ?", nativeQuery = true)
    Bidding findBiddingByAuction(int auction_id);
}
