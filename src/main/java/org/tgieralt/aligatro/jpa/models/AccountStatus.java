package org.tgieralt.aligatro.jpa.models;

public enum AccountStatus {
    ACTIVE,
    INACTIVE,
    BLOCKED
}
