package org.tgieralt.aligatro.jpa.models;

public enum AuctionCategory {
    CLOTHING ("CLOTHING"),
    FOODS ("FOODS"),
    MOBILE_PHONES ("MOBILE PHONES"),
    TOYS ("TOYS"),
    ELECTRONICS ("ELECTRONICS"),
    TOOLS ("TOOLS"),
    HOME_AND_GARDEN ("HOME AND GARDEN"),
    HOME_APPLIANCES ("HOME APPLIANCES"),
    BAGS_AND_SHOES ("BAGS AND SHOES"),
    JEWELERY ("JEWELERY"),
    WATCH ("WATCH"),
    HEALTH_AND_BEAUTY ("HEALTH AND BEAUTY"),
    COMPUTERS ("COMPUTERS"),
    SPORTS_AND_OUTDOOR ("SPORTS AND OUTDOOR"),
    AUTOMOBILES ("AUTOMOBILES"),
    MOTORCYCLES ("MOTORCYCLES"),
    LIGHTS_AND_LIGHTNING ("LIGHTS AND LIGHTNING"),
    ENTERTAINMENT ("ENTERTAINMENT"),
    SERVICES ("SERVICES");


    private String category;

    AuctionCategory(String category) {
        this.category = category;
    }

    public static AuctionCategory[] getValues() {
         return AuctionCategory.values();
    }

    public String getCategory() {
        return category;
    }
}
