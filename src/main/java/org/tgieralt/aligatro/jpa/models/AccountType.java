package org.tgieralt.aligatro.jpa.models;

public enum AccountType {
    NORMAL,
    PREMIUM
}
