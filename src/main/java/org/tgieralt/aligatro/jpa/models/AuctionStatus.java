package org.tgieralt.aligatro.jpa.models;

public enum AuctionStatus {
    ACTIVE,
    INACTIVE,
    FINISHED
}
