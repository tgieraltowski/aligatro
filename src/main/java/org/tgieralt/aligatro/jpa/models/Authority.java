package org.tgieralt.aligatro.jpa.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "authorities")
public class Authority implements Serializable {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "users_username")
    private User user;
    @Id
    private String username;
    private String authority;

    public Authority() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
