package org.tgieralt.aligatro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.tgieralt.aligatro.cache.CacheKeyGenerator;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.tgieralt.aligatro.jpa")
@EnableCaching
public class AligatroApplication {

    public static void main(String[] args) {
        SpringApplication.run(AligatroApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Bean("cacheKeyGenerator")
    public KeyGenerator keyGenerator() {
        return new CacheKeyGenerator();
    }

}
