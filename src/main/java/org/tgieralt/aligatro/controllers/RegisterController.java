package org.tgieralt.aligatro.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.tgieralt.aligatro.jpa.models.User;
import org.tgieralt.aligatro.services.UserDetailsService;
import org.tgieralt.aligatro.services.UserService;
import org.tgieralt.aligatro.validation.forms.UserDetailsForm;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.*;

@Controller
public class RegisterController {
    private static final Logger logger = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;
    @Autowired
    private UserDetailsService userDetailsService;

    @GetMapping("/register")
    public String displayRegisterForm(Model model) {
        logger.info("Getting GET request for /register.");
        return "register";
    }

    @PostMapping(value = "/register/new")
    public String registerNewUser(Model model, HttpSession session, @RequestParam String username
            , @RequestParam String password, UserDetailsForm userDetailsForm) {
        logger.info("Getting POST request from /register/new.");
        User user = userService.saveNewUser(username, passwordEncoder.encode(password));
        session.setAttribute("user", user);
        model.addAttribute(userDetailsForm);
        return "userDetails";
    }

    @PostMapping("register/details")
    public String checkUserDetails(@Valid UserDetailsForm userDetailsForm, BindingResult bindingResult
            , HttpSession session, @RequestParam MultipartFile avatar) {
        if (bindingResult.hasErrors()) {
            return "userDetails";
        }
        saveUserDetailsFromForm(userDetailsForm, session, avatar);
        return "index";
    }

    @GetMapping("/edit")
    public String editUserData(UserDetailsForm userDetailsForm, Model model, Authentication authentication) {
        String username = authentication.getName();
        User user = userService.findUserByUsername(username);
        model.addAttribute(userDetailsForm);
        model.addAttribute("user", user);
        return "editUserDetails";
    }

    @PostMapping("/edit")
    public String validateEditUserData(@Valid UserDetailsForm userDetailsForm, BindingResult bindingResult
            , HttpSession session, @RequestParam MultipartFile avatar, Model model, Authentication authentication) {
        String username = authentication.getName();
        User user = userService.findUserByUsername(username);
        session.setAttribute("user", user);
        if (bindingResult.hasErrors()) {
            model.addAttribute("user", user);
            return "editUserDetails";
        }
        editUserDetailsFromForm(userDetailsForm, session, avatar);
        return "redirect:/";
    }

    private void saveUserDetailsFromForm(UserDetailsForm userDetailsForm, HttpSession session, MultipartFile avatar) {
        String email = userDetailsForm.getEmail();
        String province = userDetailsForm.getProvince();
        String city = userDetailsForm.getCity();
        String street = userDetailsForm.getStreet();
        int houseNumber = Integer.parseInt(userDetailsForm.getHouseNumber());
        int apartmentNumber = Integer.parseInt(userDetailsForm.getApartmentNumber());
        String postCode = userDetailsForm.getPostCode();
        byte[] avatarByteArray;

        try {
            if (avatar.isEmpty()) {
                avatarByteArray = createDefaultAvatar();
            } else {
                avatarByteArray = avatar.getBytes();
            }
            userDetailsService.saveUserDetails(email, province, city, street, houseNumber, apartmentNumber, postCode, (User)session.getAttribute("user"), avatarByteArray);
        } catch (IOException e) {
            logger.error("Couldn't save UserDetails due to avatar not being valid byte[].");
        }
    }

    private void editUserDetailsFromForm(UserDetailsForm userDetailsForm, HttpSession session, MultipartFile avatar) {
        try {
            userDetailsService.updateUserDetails(userDetailsForm, session, avatar);
        } catch (IOException e) {
            logger.error("Couldn't save UserDetails due to avatar not being valid byte[].");
        }
    }

    private byte[] createDefaultAvatar() throws IOException {
        BufferedImage bufferedImage = ImageIO.read(new File("src/main/resources/images/noavatar-300x300.jpg"));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "jpg", bos);
        return bos.toByteArray();
    }
}
