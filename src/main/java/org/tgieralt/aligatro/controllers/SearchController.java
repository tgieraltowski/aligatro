package org.tgieralt.aligatro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import org.tgieralt.aligatro.jpa.models.Auction;
import org.tgieralt.aligatro.services.AuctionService;

import java.util.Optional;

@Controller
public class SearchController {
    @Autowired
    private AuctionService auctionService;

    @PostMapping("/search")
    public RedirectView searchPhrase(@RequestParam String phrase) {
        Optional<Auction> auctionOptional = auctionService.findAuctionByPhrase(phrase);
        if (auctionOptional.isPresent()) {
            int auctionId = auctionOptional.get().getId();
            return new RedirectView("/auction/details/" + auctionId);
        } else {
            return new RedirectView("/notFound");
        }
    }

    @GetMapping("/notFound")
    public String searchNotFound(Model model) {
        return "notFound";
    }
}
