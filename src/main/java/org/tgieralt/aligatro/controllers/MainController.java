package org.tgieralt.aligatro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.tgieralt.aligatro.jpa.models.Auction;
import org.tgieralt.aligatro.jpa.models.AuctionStatus;
import org.tgieralt.aligatro.jpa.models.Purchase;
import org.tgieralt.aligatro.services.AuctionClosingService;
import org.tgieralt.aligatro.services.AuctionService;
import org.tgieralt.aligatro.services.PurchaseService;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class MainController {
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private AuctionClosingService auctionClosingService;

    @GetMapping("/")
    @Transactional
    public String showMainPage(Model model, Authentication authentication
            , @RequestParam(value = "category") Optional<String> category
            , @RequestParam(value = "dateSort") Optional<String> dateSort
            , @RequestParam(value = "currentUsers") Optional<String> currentUsers) {
        auctionClosingService.closeFinishedAuctions();
        List<Auction> allAuctions = auctionService.getAllAuctions();
        allAuctions = filterCategory(category, allAuctions);
        allAuctions = filterDate(dateSort, allAuctions);
        allAuctions = filterUsers(currentUsers, allAuctions, authentication);
        model.addAttribute("allAuctions", allAuctions);
        return "index";
    }

    @GetMapping("/myPurchases")
    public String showMyPurchases(Model model, Authentication authentication) {
        List<Purchase> purchases = purchaseService.getMyPurchasesList(authentication.getName());
        model.addAttribute("purchases", purchases);
        return "myPurchases";
    }

    @GetMapping("/myAuctions")
    public String showMyAuctions(Model model, Authentication authentication) {
        List<Auction> auctions = auctionService.getAuctionsByUser(authentication.getName());
        model.addAttribute("auctions", auctions);
        return "myAuctions";
    }

    private List<Auction> filterUsers(Optional<String> currentUsers, List<Auction> allAuctions, Authentication authentication) {
        if (currentUsers.isPresent()) {
            switch (currentUsers.get()) {
                case "currentUsers":
                    return allAuctions.stream()
                            .filter(e -> e.getUser().getUsername().equals(authentication.getName()))
                            .filter(e -> e.getAuctionStatus().equals(AuctionStatus.ACTIVE))
                            .collect(Collectors.toList());
                case "otherUsers":
                    return allAuctions.stream()
                            .filter(e -> !e.getUser().getUsername().equals(authentication.getName()))
                            .filter(e -> e.getAuctionStatus().equals(AuctionStatus.ACTIVE))
                            .collect(Collectors.toList());
                default:
                    return allAuctions;
            }
        }
        return allAuctions;
    }

    private List<Auction> filterDate(Optional<String> dateSort, List<Auction> allAuctions) {
        if (dateSort.isPresent()) {
           switch (dateSort.get()) {
               case "latest10":
                   return allAuctions.stream()
                           .sorted(Comparator.comparing(Auction::getStartDate).reversed())
                           .filter(e -> e.getAuctionStatus().equals(AuctionStatus.ACTIVE))
                           .limit(10)
                           .collect(Collectors.toList());
               case "soon10":
                   return allAuctions.stream()
                           .sorted(Comparator.comparing(Auction::getEndDate))
                           .filter(e -> e.getAuctionStatus().equals(AuctionStatus.ACTIVE))
                           .limit(10)
                           .collect(Collectors.toList());
               case "today":
                   return allAuctions.stream()
                           .filter(e -> e.getEndDate().equals(LocalDate.now()))
                           .filter(e -> e.getAuctionStatus().equals(AuctionStatus.ACTIVE))
                           .collect(Collectors.toList());
               default:
                   return allAuctions;
           }
        }
        return allAuctions;
    }

    private List<Auction> filterCategory(Optional<String> category, List<Auction> allAuctions) {
        return category.map(s -> allAuctions.stream()
                .filter(e -> e.getAuctionCategory().toString().equalsIgnoreCase(s))
                .filter(e -> e.getAuctionStatus().equals(AuctionStatus.ACTIVE))
                .collect(Collectors.toList())).orElse(allAuctions);
    }
}
