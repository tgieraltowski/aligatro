package org.tgieralt.aligatro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;
import org.tgieralt.aligatro.jpa.models.Auction;
import org.tgieralt.aligatro.jpa.models.Purchase;
import org.tgieralt.aligatro.jpa.models.User;
import org.tgieralt.aligatro.services.AuctionService;
import org.tgieralt.aligatro.services.PurchaseService;
import org.tgieralt.aligatro.services.UserService;
import org.tgieralt.aligatro.validation.forms.BiddingForm;
import org.tgieralt.aligatro.validation.forms.PurchaseForm;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
public class AuctionController {
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private UserService userService;

    @GetMapping("/addAuction")
    public String createNewAuction(Model model, Authentication authentication) {
        User user = userService.findUserByUsername(authentication.getName());
        model.addAttribute("user", user);
        return "createAuction";
    }

    @PostMapping("/addAuction/create")
    public RedirectView processNewAuctionForm(@RequestParam String title, @RequestParam String description, @RequestParam String category,
                                              @RequestParam String endDate, @RequestParam String minimumBid, @RequestParam String buyNow,
                                              @RequestParam(defaultValue = "false") String promoted, @RequestParam MultipartFile image, Authentication authentication) throws IOException {
        auctionService.createNewAuction(title, description, category, endDate, minimumBid, buyNow, promoted, image, authentication);
        return new RedirectView("/");
    }

    @GetMapping("/auction/getImage/{id}")
    public void getAuctionImage(HttpServletResponse response, @PathVariable String id) throws IOException {
        byte[] image = auctionService.getAuctionImageById(Integer.parseInt(id));
        response.setContentType("image/jpg");
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(image);
        outputStream.close();
    }

    @GetMapping("/auction/details/{id}")
    public String showAuctionDetails(Model model, @PathVariable String id, BiddingForm biddingForm) {
        Auction auction = auctionService.getAuctionById(Integer.parseInt(id));
        auctionService.increaseViews(Integer.parseInt(id));
        model.addAttribute(biddingForm);
        model.addAttribute("auction", auction);
        return "auctionDetails";
    }

    @PostMapping("/auction/bid")
    public String bidOnAuction(@RequestParam String auctionId, @Valid BiddingForm biddingForm
            , BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "redirect:/auction/details/"+auctionId;
        }
        auctionService.addNewBid(Integer.parseInt(auctionId), biddingForm.getBid(), authentication);
        return "redirect:/auction/details/"+auctionId;
    }

    @GetMapping("/auction/buyNow/{auctionId}")
    public String makeBuyNowPurchase(@PathVariable String auctionId, Model model, PurchaseForm purchaseForm) {
        Auction auction = auctionService.getAuctionById(Integer.parseInt(auctionId));
        model.addAttribute("auction", auction);
        model.addAttribute(purchaseForm);
        return "purchase";
    }

    @PostMapping("/auction/buyNow")
    public String validatePurchase(@RequestParam String auctionId, @Valid PurchaseForm purchaseForm
            , BindingResult bindingResult, Model model, Authentication authentication) {
        Auction auction = auctionService.getAuctionById(Integer.parseInt(auctionId));
        if (bindingResult.hasErrors()) {
            model.addAttribute("auction", auction);
            return "purchase";
        }
        List<Purchase> purchases = purchaseService.confirmPurchase(purchaseForm, auction.getId(), authentication);
        model.addAttribute("purchases", purchases);
        return "myPurchases";
    }

    @GetMapping("/auction/payNow/{auctionId}")
    public String payNowForFinishedAuction(@PathVariable String auctionId, PurchaseForm purchaseForm, Model model) {
        Auction auction = auctionService.getAuctionById(Integer.parseInt(auctionId));
        model.addAttribute("auction", auction);
        model.addAttribute(purchaseForm);
        return "payNow";
    }
    
    @PostMapping("/auction/payNow")
    public String validatePayment(@RequestParam String auctionId, @Valid PurchaseForm purchaseForm
            , BindingResult bindingResult, Model model, Authentication authentication) {
        Auction auction = auctionService.getAuctionById(Integer.parseInt(auctionId));
        if (bindingResult.hasErrors()) {
            model.addAttribute("auction", auction);
            return "payNow";
        }
        purchaseService.confirmPayment(purchaseForm, auction.getId(), authentication);
        List<Purchase> purchases = purchaseService.getMyPurchasesList(authentication.getName());
        model.addAttribute("purchases", purchases);
        return "myPurchases";
    }
    
    @PostMapping("/auction/cancel")
    public String cancelAuction(@RequestParam String auctionId, Model model) {
        boolean success = auctionService.cancelAuction(auctionId);
        model.addAttribute("auctionId", auctionId);
        if (success) {            
            return "auctionCancelled";
        }
        return "auctionNotCancelled";
    }
}
