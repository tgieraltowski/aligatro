package org.tgieralt.aligatro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.tgieralt.aligatro.services.UserDetailsService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class LoginController {
    @Autowired
    private UserDetailsService userDetailsService;

    @GetMapping("/login")
    public String displayLoginPage() {
        return "login";
    }

    @GetMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    @GetMapping("/avatar")
    public void getUsersAvatar(HttpServletResponse response, Authentication authentication) throws IOException {
        String username = authentication.getName();
        byte[] avatar = userDetailsService.getUsersAvatarImage(username);
        response.setContentType("image/jpg");
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(avatar);
        outputStream.close();
    }
}
