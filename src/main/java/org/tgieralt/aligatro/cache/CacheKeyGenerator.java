package org.tgieralt.aligatro.cache;

import java.lang.reflect.Method;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.util.StringUtils;

public class CacheKeyGenerator implements KeyGenerator {

    @Override
    public Object generate(Object o, Method method, Object... os) {
        return o.getClass().getSimpleName()+"_"
                + method.getName()+"_"
                + StringUtils.arrayToDelimitedString(os, "_");
    }
    
}
