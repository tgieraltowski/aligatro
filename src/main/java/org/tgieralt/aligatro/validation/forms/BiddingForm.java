package org.tgieralt.aligatro.validation.forms;

import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

public class BiddingForm {
    @NotNull(message = "You must enter your bid.")
    @DecimalMin(value = "0.01", inclusive = true, message = "Minimal bid is 0.01 PLN")
    @DecimalMax(value = "9999999.99", inclusive = true, message = "Maximum bid is 9999999.99 PLN")
    @NumberFormat(style = NumberFormat.Style.CURRENCY)
    private Double bid;

    public Double getBid() {
        return bid;
    }

    public void setBid(Double bid) {
        this.bid = bid;
    }
}
