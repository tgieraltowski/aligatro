package org.tgieralt.aligatro.validation.forms;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

public class UserDetailsForm {
    @Size(min = 1, message = "Email is mandatory field.")
    @Email(message = "Email must be valid.")
    private String email;
    @Size(min = 1, message = "Province is mandatory field.")
    @Length(max = 40, message = "Province must be 40 characters max.")
    private String province;
    @Size(min = 1, message = "City is mandatory field.")
    @Length(max = 40, message = "City must be 40 characters max.")
    private String city;
    @Size(min = 1, message = "Street is mandatory field.")
    @Length(max = 40, message = "Street is 40 characters max.")
    private String street;
    @NotNull(message = "House Number is mandatory field.")
    @Pattern(regexp = "\\d{1,4}", message = "Enter House Number from 1 to 9999")
    private String houseNumber;
    @NotNull(message = "Apartment Number is mandatory field.")
    @Pattern(regexp = "\\d{1,4}", message = "For a standalone house enter apartment number: 0")
    private String apartmentNumber;
    @Pattern(regexp = "\\d{2}-\\d{3}", message = "Post Code must match DD-DDD pattern")
    private String postCode;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(String apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

}
