package org.tgieralt.aligatro.validation.forms;

import javax.validation.constraints.Pattern;

public class PurchaseForm {

    @Pattern(regexp = "\\d{4}-\\d{4}-\\d{4}-\\d{4}", message = "Credit card number must be in format ####-####-####-####")
    private String creditCard;
    @Pattern(regexp = "\\d{3}", message = "CSV number must be in format ###")
    private String csv;

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getCsv() {
        return csv;
    }

    public void setCsv(String csv) {
        this.csv = csv;
    }
}
