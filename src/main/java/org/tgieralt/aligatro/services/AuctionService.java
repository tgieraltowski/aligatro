package org.tgieralt.aligatro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.tgieralt.aligatro.jpa.models.*;
import org.tgieralt.aligatro.jpa.repositories.AuctionRepository;
import org.tgieralt.aligatro.jpa.repositories.BiddingRepository;
import org.tgieralt.aligatro.jpa.repositories.UserRepository;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

@Service
public class AuctionService {
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BiddingRepository biddingRepository;

    @CacheEvict(value = "auctions", allEntries = true)
    public void createNewAuction(String title, String description, String category, String endDate, String minimumBid,
                                 String buyNow, String promoted, MultipartFile image, Authentication authentication) throws IOException {
        Auction auction = new Auction();
        auction.setTitle(title);
        auction.setDescription(description);
        auction.setAuctionCategory(AuctionCategory.valueOf(category));
        auction.setStartDate(LocalDate.now());
        auction.setEndDate(LocalDate.parse(endDate));
        auction.setMinimumBid(Double.parseDouble(minimumBid));
        auction.setBuyNowPrice(Double.parseDouble(buyNow));
        auction.setPromoted(Boolean.parseBoolean(promoted));
        auction.setAuctionStatus(AuctionStatus.ACTIVE);
        String username = authentication.getName();
        Optional<User> loggedUser = userRepository.findById(username);
        auction.setCity(loggedUser.orElse(null).getUserDetails().getAddress().getCity());
        auction.setPicture(image.getBytes());
        auction.setViews(0L);
        loggedUser.get().addAuction(auction);
        auctionRepository.save(auction);
        userRepository.save(loggedUser.get());
        Bidding bidding = new Bidding();
        bidding.setAuction(auction);
        bidding.setCurrentBid(0.0);
        biddingRepository.save(bidding);
    }
    
    @Cacheable(value = "auctions", keyGenerator = "cacheKeyGenerator")    
    public List<Auction> getAllAuctions() {
        List<Auction> auctionList = new ArrayList<>();
        Iterable<Auction> auctions = auctionRepository.findAll();
        auctions.forEach(auctionList::add);
        return auctionList;
    }
    
    @Cacheable(value = "images", key = "{#id}")
    public byte[] getAuctionImageById(int id) {
        Optional<Auction> auction = auctionRepository.findById(id);
        byte[] picture = auction.get().getPicture();
        return picture;
    }

    public Auction getAuctionById(int id) {
        Optional<Auction> auctionOptional = auctionRepository.findById(id);
        return auctionOptional.get();
    }

    public void increaseViews(int id) {
        Auction auction = auctionRepository.findById(id).get();
        auction.setViews(auction.getViews()+1);
        auctionRepository.save(auction);
    }

    @CacheEvict(value = "auctions", allEntries = true)
    public void addNewBid(int auctionId, Double bid, Authentication authentication) {
        Auction auction = auctionRepository.findById(auctionId).get();
        Bidding bidding = biddingRepository.findBiddingByAuction(auctionId);
        User user = userRepository.findById(authentication.getName()).get();
        if (bid <= bidding.getCurrentBid() || auction.getMinimumBid() > bid) {
            return;
        }
        bidding.setCurrentBid(bid);
        user.addBidding(bidding);
        biddingRepository.save(bidding);
    }

    public List<Auction> getAuctionsByUser(String username) {
        return auctionRepository.findAllByUserUsername(username);
    }

    public Optional<Auction> findAuctionByPhrase(String phrase) {
        Optional<Auction> auctionOptional;
        auctionOptional = auctionRepository.searchPhraseInTitle(phrase);
        if (auctionOptional.isPresent()) {
            return auctionOptional;
        }

        List<Auction> auctions = auctionRepository.findAllByUserUsername(phrase);
        if (auctions.size() > 0) {
            auctionOptional = Optional.of(auctions.get(0));
            return auctionOptional;
        }

        auctionOptional = auctionRepository.searchPhraseInDescription(phrase);
        if (auctionOptional.isPresent()) {
            return auctionOptional;
        }

        return auctionOptional;
    }

    @Transactional
    @CacheEvict(value = "auctions", allEntries = true)
    public boolean cancelAuction(String auctionId) {
        int auctionIdNumber = Integer.parseInt(auctionId);
        Bidding bidding = biddingRepository.findBiddingByAuction(auctionIdNumber);
        biddingRepository.delete(bidding);
        Optional<Auction> auction = auctionRepository.findById(auctionIdNumber);
        Optional<User> user = userRepository.findById(auction.get().getUser().getUsername());
        user.get().removeAuction(auction.get());
        userRepository.save(user.get());
        auctionRepository.deleteById(Integer.parseInt(auctionId));
        
        Optional<Auction> auctionOpt = auctionRepository.findById(auctionIdNumber);
        if (auctionOpt.isPresent()) {
            return false;
        }
        return true;
    }
}
