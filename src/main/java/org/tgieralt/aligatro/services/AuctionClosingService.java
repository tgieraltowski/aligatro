package org.tgieralt.aligatro.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tgieralt.aligatro.jpa.models.Auction;
import org.tgieralt.aligatro.jpa.models.AuctionStatus;
import org.tgieralt.aligatro.jpa.models.Purchase;
import org.tgieralt.aligatro.jpa.models.User;
import org.tgieralt.aligatro.jpa.repositories.AuctionRepository;
import org.tgieralt.aligatro.jpa.repositories.PurchaseRepository;

import java.time.LocalDate;
import java.util.List;

@Service
public class AuctionClosingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuctionClosingService.class);

    @Autowired
    private AuctionRepository auctionRepository;

    @Autowired
    private PurchaseRepository purchaseRepository;

    public void closeFinishedAuctions() {
        List<Auction> auctions = auctionRepository.findAllMaturedAuctions();
        for (Auction auction : auctions) {
            if (auction.getBidding().getCurrentBid() >= auction.getMinimumBid()) {
                closeAuctionWithSell(auction);
            } else {
                closeAuctionWithNoSell(auction);
            }
        }
    }

    private void closeAuctionWithSell(Auction auction) {
        User buyer = auction.getBidding().getUser();
        Purchase purchase = new Purchase();

        auction.setAuctionStatus(AuctionStatus.FINISHED);
        purchase.setPurchaseDate(LocalDate.now());
        purchase.setAmount(auction.getBidding().getCurrentBid());
        purchase.setPaid(false);
        buyer.addPurchase(purchase);
        auction.setPurchase(purchase);

        purchaseRepository.save(purchase);
        auctionRepository.save(auction);
        LOGGER.info("Auction " + auction.getId() + " has been automatically closed at " + LocalDate.now()
                + ". Auction winner is " + buyer.getUsername());

    }

    private void closeAuctionWithNoSell(Auction auction) {
        auction.setAuctionStatus(AuctionStatus.FINISHED);
        auctionRepository.save(auction);
        LOGGER.info("Auction " + auction.getId() + " has been automatically closed at " + LocalDate.now()
                + ". There was no bid exceeding minimal requirements.");
    }
}
