package org.tgieralt.aligatro.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.tgieralt.aligatro.jpa.models.*;
import org.tgieralt.aligatro.jpa.repositories.UserDetailsRepository;
import org.tgieralt.aligatro.jpa.repositories.UserRepository;
import org.tgieralt.aligatro.validation.forms.UserDetailsForm;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;

@Service
public class UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(UserDetailsService.class);
    private UserDetailsRepository userDetailsRepository;
    private UserRepository userRepository;

    @Autowired
    public UserDetailsService(UserDetailsRepository userDetailsRepository, UserRepository userRepository) {
        this.userDetailsRepository = userDetailsRepository;
        this.userRepository = userRepository;
    }

    public UserDetails saveUserDetails(String email, String province, String city, String street, int houseNumber, int apartmentNumber, String postCode, User user, byte[] avatar) {
        logger.info("Trying to save UserDetails for User: " + user.getUsername());
        UserDetails userDetails = new UserDetails();
        userDetails.setEmail(email);
        Address address = new Address(province, city, street, houseNumber, apartmentNumber, postCode);
        userDetails.setAddress(address);
        userDetails.setAccountCreationDate(LocalDate.now());
        userDetails.setAccountStatus(AccountStatus.ACTIVE);
        userDetails.setAccountType(AccountType.NORMAL);
        userDetails.setAvatar(avatar);
        user.setUserDetails(userDetails);
        userDetailsRepository.save(userDetails);
        userRepository.save(user);
        return userDetails;
    }

    public byte[] getUsersAvatarImage(String username) {
        UserDetails userDetails = userDetailsRepository.findUsersDetailsByUsername(username);
        return userDetails.getAvatar();
    }

    public UserDetails updateUserDetails(UserDetailsForm userDetailsForm, HttpSession session, MultipartFile avatar) throws IOException {
        UserDetails userDetails = userDetailsRepository.findUsersDetailsByUsername(((User) session.getAttribute("user")).getUsername());
        userDetails.setEmail(userDetailsForm.getEmail());
        Address address = new Address(userDetailsForm.getProvince()
                , userDetailsForm.getCity()
                , userDetailsForm.getStreet()
                , Integer.parseInt(userDetailsForm.getHouseNumber())
                , Integer.parseInt(userDetailsForm.getApartmentNumber())
                , userDetailsForm.getPostCode());
        userDetails.setAddress(address);
        userDetails.setAvatar(avatar.getBytes());
        userDetailsRepository.save(userDetails);
        return userDetails;
    }
}
