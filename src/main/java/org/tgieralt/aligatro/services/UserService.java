package org.tgieralt.aligatro.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tgieralt.aligatro.jpa.models.Authority;
import org.tgieralt.aligatro.jpa.models.User;
import org.tgieralt.aligatro.jpa.repositories.AuthorityRepository;
import org.tgieralt.aligatro.jpa.repositories.UserRepository;

@Service
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserRepository userRepository;
    private AuthorityRepository authorityRepository;

    @Autowired
    public UserService(UserRepository userRepository, AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
    }

    public User saveNewUser(String username, String hashedPassword) {
        logger.info("Trying to save new user: " + username);
        Authority authority = new Authority();
        authority.setAuthority("normalUser");
        User user = new User();
        user.setUsername(username);
        user.setPassword(hashedPassword);
        user.setEnabled(true);
        authority.setUsername(username);
        user.setAuthority(authority);
        userRepository.save(user);
        authorityRepository.save(authority);
        return user;
    }

    public User findUserByUsername(String username) {
        return userRepository.findById(username).get();
    }
}
