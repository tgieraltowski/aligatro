package org.tgieralt.aligatro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.tgieralt.aligatro.jpa.models.Auction;
import org.tgieralt.aligatro.jpa.models.AuctionStatus;
import org.tgieralt.aligatro.jpa.models.Purchase;
import org.tgieralt.aligatro.jpa.models.User;
import org.tgieralt.aligatro.jpa.repositories.AuctionRepository;
import org.tgieralt.aligatro.jpa.repositories.PurchaseRepository;
import org.tgieralt.aligatro.jpa.repositories.UserRepository;
import org.tgieralt.aligatro.validation.forms.PurchaseForm;

import java.time.LocalDate;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;

@Service
public class PurchaseService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseService.class);
    
    @Autowired
    private PurchaseRepository purchaseRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuctionRepository auctionRepository;

    @CacheEvict(value = "auctions", allEntries = true)
    public List<Purchase> confirmPurchase(PurchaseForm purchaseForm, int auctionId, Authentication authentication) {
        Purchase purchase = new Purchase();
        User user = userRepository.findById(authentication.getName()).get();
        Auction auction = auctionRepository.findById(auctionId).get();
        purchase.setAmount(auction.getBuyNowPrice());
        purchase.setCreditCardNumber(purchaseForm.getCreditCard());
        purchase.setPurchaseDate(LocalDate.now());
        purchase.setPaid(true);
        user.addPurchase(purchase);
        auction.setAuctionStatus(AuctionStatus.FINISHED);
        auction.setPurchase(purchase);
        purchaseRepository.save(purchase);
        auctionRepository.save(auction);
        return user.getPurchases();
    }

    public List<Purchase> getMyPurchasesList(String username) {
        return purchaseRepository.findAllByUserUsername(username);
    }

    public void confirmPayment(PurchaseForm purchaseForm, int auctionId, Authentication authentication) {
        Auction auction = auctionRepository.findById(auctionId).get();
        Purchase purchase = auction.getPurchase();
        purchase.setCreditCardNumber(purchaseForm.getCreditCard());
        purchase.setPurchaseDate(LocalDate.now());
        purchase.setPaid(true);
        auctionRepository.save(auction);
    }
}
