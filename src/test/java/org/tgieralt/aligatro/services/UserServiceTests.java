package org.tgieralt.aligatro.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.tgieralt.aligatro.jpa.models.User;
import org.tgieralt.aligatro.jpa.repositories.AuthorityRepository;
import org.tgieralt.aligatro.jpa.repositories.UserRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserServiceTests {

    private UserService userService;
    private UserRepository userRepository = Mockito.mock(UserRepository.class);
    private AuthorityRepository authorityRepository = Mockito.mock(AuthorityRepository.class);
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Before
    public void initialize() {
        userService = new UserService(userRepository, authorityRepository);
        bCryptPasswordEncoder = new BCryptPasswordEncoder();
    }

    @Test
    public void savedUserHasProperlyEncodedPassword() {
        String password = "testPassword";
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(new User());
        User savedUser = userService.saveNewUser("testUser", bCryptPasswordEncoder.encode("testPassword"));
        assertTrue(bCryptPasswordEncoder.matches(password, savedUser.getPassword()));
    }

    @Test
    public void savedNewUserHasNormalUserAuthoritySet() {
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(new User());
        User savedUser = userService.saveNewUser("testUser", bCryptPasswordEncoder.encode("testPassword"));
        assertEquals("normalUser", savedUser.getAuthority().getAuthority());
    }

    @Test
    public void savedNewUserIsEnabled() {
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(new User());
        User savedUser = userService.saveNewUser("testUser", bCryptPasswordEncoder.encode("testPassword"));
        assertTrue(savedUser.isEnabled());
    }

}
