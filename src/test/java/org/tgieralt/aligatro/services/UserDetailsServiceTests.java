package org.tgieralt.aligatro.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.tgieralt.aligatro.jpa.models.AccountStatus;
import org.tgieralt.aligatro.jpa.models.AccountType;
import org.tgieralt.aligatro.jpa.models.User;
import org.tgieralt.aligatro.jpa.models.UserDetails;
import org.tgieralt.aligatro.jpa.repositories.UserDetailsRepository;
import org.tgieralt.aligatro.jpa.repositories.UserRepository;

import java.io.IOException;
import java.time.LocalDate;

public class UserDetailsServiceTests {

    private UserDetailsService userDetailsService;
    private UserDetailsRepository userDetailsRepository = Mockito.mock(UserDetailsRepository.class);
    private UserRepository userRepository = Mockito.mock(UserRepository.class);

    private String email = "email@gmail.com";
    private String province = "TestProvince";
    private String city = "TestCity";
    private String street = "TestStreet";
    private int houseNumber = 1;
    private int apartmentNumber = 1;
    private String postCode = "11-111";
    private User user = new User();
    private MultipartFile avatar = new MockMultipartFile("mockedAvatarFile", new byte[]{});
    private byte[] avatarByteArray;

    @Before
    public void initialize() throws IOException {
        userDetailsService = new UserDetailsService(userDetailsRepository, userRepository);
        avatarByteArray = avatar.getBytes();
    }

    @Test
    public void savedDetailsHaveCreationDateSet() {
        UserDetails userDetails = userDetailsService.saveUserDetails(email, province, city, street, houseNumber, apartmentNumber, postCode, user, avatarByteArray);
        Assert.assertEquals(LocalDate.now(), userDetails.getAccountCreationDate());
    }

    @Test
    public void savedDetailsHaveAccountStatusSetToActive() {
        UserDetails userDetails = userDetailsService.saveUserDetails(email, province, city, street, houseNumber, apartmentNumber, postCode, user, avatarByteArray);
        Assert.assertEquals(AccountStatus.ACTIVE, userDetails.getAccountStatus());
    }

    @Test
    public void savedDetailsHaveAccountTypeSetToNormal() {
        UserDetails userDetails = userDetailsService.saveUserDetails(email, province, city, street, houseNumber, apartmentNumber, postCode, user, avatarByteArray);
        Assert.assertEquals(AccountType.NORMAL, userDetails.getAccountType());
    }
}
